
# 💡☁️ Deploy from GitLab to Clever Cloud

This is a simple example of CI/CD component to deploy from GitLab to Clever Cloud using [clever-tools](https://www.clever-cloud.com/doc/getting-started/cli/)

## Deploy from gitlab.com

Add the following snippet to your `.gitlab-ci.yml` file to use in your pipeline:

```yaml
include:
  - component: gitlab.com/CleverCloud/clever-cloud-pipeline/deploy-to-prod@~latest
```

Add inputs to override the default values. For example:

```yaml
include:
 - component: gitlab.com/CleverCloud/clever-cloud-pipeline/deploy-to-prod@~latest
    inputs:
      stage: test
```

## Deploy from your self-hosted GitLab instance

You can use this component in your self-hosted GitLab instance. Follow [these instructions](https://docs.gitlab.com/ee/ci/components/index.html#use-a-gitlabcom-component-in-a-self-managed-instance) to set it up.

## Component workflow

This script does the following:

- download clever-tools (our CLI) in a Node container
- applies a rule to only execute the workflow if we committed to the default branch (in the case of our project, it's `main`), which you can set in your GitLab project in **Settings > Repository > "Branch defaults"**
- deploys the commit on your production app

## Environment Variables

You need to set some variables in order to be able to use clever-tools in GitLab jobs. Go to your project's **Settings > CI/CD > "Variables"** and add the following variables:

- `CLEVER_TOKEN` AND `CLEVER_SECRET` (find it in your machine, usually in `~/.config/clever-cloud/clever-tools.json`)
- `APP_ID` to avoid committing it (find it at the top right in Clever Cloud Console, in your application tab).

These are the default inputs to run this job. You can modify them to fit your needs.

More info can be found in [GitLab's doc](https://docs.gitlab.com/ee/topics/build_your_application.html)
